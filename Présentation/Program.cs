﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Présentation
{
    class Program
    {
        static void Main(string[] args)
        {
            //MACHINE A SOUS

            #region Déclaration des variables

            float argent = 50.0f;
            float mise = 0.0f;
            string[] tableauDesRoues = new string[] { "Cerise", "Citron", "Poire", "Bonus", "JACKPOT" };
            bool jeu = true;
            bool gameover = false;
            string option = "";
            int choix = 0;
            string[] Joueur = new string[1];
            string[] InfosJoueurs = new string[1];
            string pseudo = "";
            string mdp = "";
            string infos = "";
            List<string[]> ListeJoueurs = new List<string[]>();

            #endregion

            #region Vérification et Récupération des Infos Joueurs

            if (File.Exists("InfosJoueurs.txt"))
            {
                Joueur = File.ReadAllLines("InfosJoueurs.txt");

                if (Joueur.Length > 0)
                {
                    foreach (string item in Joueur)
                    {
                        InfosJoueurs = item.Split(':');
                        ListeJoueurs.Add(InfosJoueurs);
                    }
                }
            }

            else
            {
                FileStream f = File.Create("InfosJoueurs.txt");
                f.Close();
            }

            #endregion

            #region Introduction

            Console.WriteLine("");
            Console.WriteLine("-1- Créer un compte");
            Console.WriteLine("-2- Se connecter");
            Console.WriteLine("");
            Console.Write("     Option : ");
            option = Console.ReadLine();

            if (int.TryParse(option, out int choixOption))
            {
                choix = int.Parse(option);
            }

            while ((!int.TryParse(option, out int choixOption2)) || (choix != 1 && choix != 2))
            {
                Console.Write("Option incorrecte : ");
                option = Console.ReadLine();

                if (int.TryParse(option, out choixOption))
                {
                    choix = int.Parse(option);
                }
            }

            Console.Clear();

            if (choix == 1)
            {
                CreationCompte();

                Console.WriteLine($"        Bonjour {pseudo} !");
                Console.WriteLine("");
                Console.WriteLine("Bienvenue dans la machine à sous Technobel !");
                Console.WriteLine("");
                Console.WriteLine("Vous disposez de 50$, à vous de placer vos mises !");
                Console.WriteLine("");
                Console.WriteLine("Cerise, Citron et Poire multiplient par 1,5 la mise si vous en avez 2,");
                Console.WriteLine("et multiplie la mise par 3 si vous obtenez les 3 !");
                Console.WriteLine("Le bonus (x2) multiplie par 3 et par 5 (x3) !");
                Console.WriteLine("Et pour terminer, le Jackpot, lui, multiplie par 5 et par 15 !");
                Console.WriteLine("");
                Console.Write("Appuyez sur [ENTER] pour lancer le jeu ...");
                Console.ReadKey();
                Console.Clear();
            }

            else if (choix == 2)
            {
                ConnexionCompte();
            }

            #endregion

            #region Fonction Pause

            void pause(int milli)
            {
                System.Threading.Thread.Sleep(milli);
            }

            #endregion

            #region Fonction Game Over

            void gameOver()
            {
                Console.WriteLine("");
                Console.WriteLine("Vous êtes à sec !");
            }

            #endregion

            #region Fonction Tour de Roue

            void tourDeRoue()
            {
                #region Variables Tour de Roue

                bool suspense = true;
                string petitPoints = " ";
                int nombreSuspense = 0;

                #endregion

                #region Boucle Suspense

                while (suspense)
                {
                    Console.WriteLine($"LA ROUE TOURNE{petitPoints}");
                    pause(150);
                    petitPoints += ".";

                    if (petitPoints == " ....")
                    {
                        petitPoints = " ";
                        nombreSuspense++;
                        Console.Clear();
                    }

                    if (nombreSuspense == 3)
                    {
                        suspense = false;
                    }

                    Console.SetCursorPosition(0, 0);

                }

                #endregion

                #region Choix et Affichage des symboles de la roue

                Console.Clear();

                suspense = true;

                var rand = new Random();

                string[] tableauRandom = new string[3];

                for (int i = 0; i < tableauRandom.Length; i++)
                {
                    tableauRandom[i] = tableauDesRoues[rand.Next(tableauDesRoues.Length)];
                }

                Console.Write($"{tableauRandom[0]}      ");
                pause(1000);
                Console.SetCursorPosition(0, 0);
                Console.Write($"{tableauRandom[0]}      {tableauRandom[1]}");
                pause(1000);
                Console.SetCursorPosition(0, 0);
                Console.WriteLine($"{tableauRandom[0]}      {tableauRandom[1]}      {tableauRandom[2]}");
                Console.WriteLine("");
                pause(2000);

                #endregion

                #region 'Si' Les 3 symboles sont identiques

                if (tableauRandom[0] == tableauRandom[1] && tableauRandom[0] == tableauRandom[2])
                {
                    switch (tableauRandom[0])
                    {
                        case "Cerise":

                            mise *= 3;
                            Console.WriteLine($"Mise multipliée par 3 ! {mise}$ gagnés !");
                            break;

                        case "Citron":

                            mise *= 3;
                            Console.WriteLine($"Mise multipliée par 3 ! {mise}$ gagnés !");
                            break;

                        case "Poire":

                            mise *= 3;
                            Console.WriteLine($"Mise multipliée par 3 ! {mise}$ gagnés !");
                            break;

                        case "Bonus":

                            mise *= 5;
                            Console.WriteLine($"Mise multipliée par 5 !! {mise}$ gagnés !");
                            break;

                        case "JACKPOT":

                            mise *= 15;
                            Console.WriteLine($"Mise multipliée par 15 !!! {mise}$ gagnés !!");
                            break;
                    }
                }

                #endregion

                #region 'Sinon si' Seulement 2 symboles sont identiques

                else if (tableauRandom[0] == tableauRandom[1] || tableauRandom[0] == tableauRandom[2] || tableauRandom[2] == tableauRandom[1])
                {
                    if (tableauRandom[0] == tableauRandom[1])
                    {
                        switch (tableauRandom[0])
                        {
                            case "Cerise":

                                mise *= 1.5f;
                                Console.WriteLine($"Mise multipliée par 1,5 ! {mise}$ gagnés !");
                                break;

                            case "Citron":

                                mise *= 1.5f;
                                Console.WriteLine($"Mise multipliée par 1,5 ! {mise}$ gagnés !");
                                break;

                            case "Poire":

                                mise *= 1.5f;
                                Console.WriteLine($"Mise multipliée par 1,5 ! {mise}$ gagnés !");
                                break;

                            case "Bonus":

                                mise *= 3;
                                Console.WriteLine($"Mise multipliée par 3 !! {mise}$ gagnés !");
                                break;

                            case "JACKPOT":

                                mise *= 5;
                                Console.WriteLine($"Mise multipliée par 5 !!! {mise}$ gagnés !!");
                                break;
                        }
                    }

                    else if (tableauRandom[0] == tableauRandom[2])
                    {
                        switch (tableauRandom[0])
                        {
                            case "Cerise":

                                mise *= 1.5f;
                                Console.WriteLine($"Mise multipliée par 1,5 ! {mise}$ gagnés !");
                                break;

                            case "Citron":

                                mise *= 1.5f;
                                Console.WriteLine($"Mise multipliée par 1,5 ! {mise}$ gagnés !");
                                break;

                            case "Poire":

                                mise *= 1.5f;
                                Console.WriteLine($"Mise multipliée par 1,5 ! {mise}$ gagnés !");
                                break;

                            case "Bonus":

                                mise *= 3;
                                Console.WriteLine($"Mise multipliée par 3 !! {mise}$ gagnés !");
                                break;

                            case "JACKPOT":

                                mise *= 5;
                                Console.WriteLine($"Mise multipliée par 5 !!! {mise}$ gagnés !!");
                                break;
                        }
                    }

                    else
                    {
                        switch (tableauRandom[1])
                        {
                            case "Cerise":

                                mise *= 1.5f;
                                Console.WriteLine($"Mise multipliée par 1,5 ! {mise}$ gagnés !");
                                break;

                            case "Citron":

                                mise *= 1.5f;
                                Console.WriteLine($"Mise multipliée par 1,5 ! {mise}$ gagnés !");
                                break;

                            case "Poire":

                                mise *= 1.5f;
                                Console.WriteLine($"Mise multipliée par 1,5 ! {mise}$ gagnés !");
                                break;

                            case "Bonus":

                                mise *= 3;
                                Console.WriteLine($"Mise multipliée par 3 !! {mise}$ gagnés !");
                                break;

                            case "JACKPOT":

                                mise *= 5;
                                Console.WriteLine($"Mise multipliée par 5 !!! {mise}$ gagnés !!");
                                break;
                        }
                    }
                }

                #endregion

                #region 'Sinon' Tout les symboles sont différents

                else
                {
                    mise *= 0;
                    Console.WriteLine("Vous n'avez rien gagné, mise perdue ...");
                }

                #endregion

                #region Recalcul de l'argent total

                argent += mise;
                argent = Convert.ToSingle((Math.Round(argent, 2)));
                mise = 0.0f;

                #endregion

                pause(2000);
                Console.Clear();
            }

            #endregion

            #region Fonction Créer un Compte

            void CreationCompte()
            {
                Console.WriteLine("");
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.WriteLine("                      ");
                Console.WriteLine("*CRÉATION D'UN COMPTE*");
                Console.WriteLine("                      ");

                pseudo = "";
                mdp = "";
                infos = "";

                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("");
                Console.Write("Veuillez entrer votre pseudo : ");
                pseudo = Console.ReadLine();
                Console.WriteLine("");

                bool verif = false;

                while (verif == false)
                {
                    verif = true;

                    foreach (string[] item in ListeJoueurs)
                    {
                        if (pseudo == item[0])
                        {
                            verif = false;

                            Console.Write("Pseudo déjà prit, veuillez en choisir un autre : ");
                            pseudo = Console.ReadLine();
                            Console.WriteLine("");
                        }
                    }
                }

                Console.WriteLine("Pseudo disponible.");
                Console.WriteLine("");
                Console.Write("Veuillez choisir un mot de passe (min. 5 char ) : ");
                mdp = Console.ReadLine();
                Console.WriteLine("");

                while (mdp.Length < 5)
                {
                    Console.Write("Veuillez choisir un autre mot de passe : ");
                    mdp = Console.ReadLine();
                    Console.WriteLine("");
                }

                infos = $"{pseudo}:{mdp}:50" + Environment.NewLine;
                File.AppendAllText("InfosJoueurs.txt", infos);
                ListeJoueurs.Add(infos.Split(':'));

                Console.BackgroundColor = ConsoleColor.Green;
                Console.ForegroundColor = ConsoleColor.Black;

                Console.WriteLine("                         ");
                Console.WriteLine("Compte crée avec succès !");
                Console.WriteLine("                         ");

                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.White;

                pause(2000);
                Console.Clear();
            }

            #endregion

            #region Fonction Se connecter à un Compte

            void ConnexionCompte()
            {
                Console.WriteLine("");
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.WriteLine("                       ");
                Console.WriteLine("*CONNEXION A UN COMPTE*");
                Console.WriteLine("                       ");
                Console.WriteLine("");
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.White;

                pseudo = "";
                mdp = "";

                Console.Write("Veuillez entrer votre pseudo : ");
                pseudo = Console.ReadLine();
                Console.WriteLine("");

                bool verif = false;

                foreach (string[] item in ListeJoueurs)
                {
                    if (pseudo == item[0])
                    {
                        verif = true;
                    }
                }

                while (verif == false)
                {
                    Console.Write("Ce pseudo n'existe pas, veuillez réessayer : ");
                    pseudo = Console.ReadLine();
                    Console.WriteLine("");

                    foreach (string[] item in ListeJoueurs)
                    {
                        if (pseudo == item[0])
                        {
                            verif = true;
                        }
                    }
                }

                Console.Write("Veuillez entrer votre mot de passe : ");
                mdp = Console.ReadLine();
                Console.WriteLine("");

                verif = false;

                foreach (string[] item in ListeJoueurs)
                {
                    if (pseudo == item[0] && mdp == item[1])
                    {
                        verif = true;
                        argent = float.Parse(item[2]);
                    }
                }

                while (verif == false)
                {
                    Console.Write("Mot de passe incorrect, veuillez réessayer : ");
                    mdp = Console.ReadLine();
                    Console.WriteLine("");

                    foreach (string[] item in ListeJoueurs)
                    {
                        if (pseudo == item[0] && mdp == item[1])
                        {
                            verif = true;
                            argent = float.Parse(item[2]);
                        }
                    }
                }
                
                Console.BackgroundColor = ConsoleColor.Green;
                Console.ForegroundColor = ConsoleColor.Black;

                Console.WriteLine("                             ");
                Console.WriteLine("Connexion au compte réussie !");
                Console.WriteLine("                             ");

                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.White;

                pause(2000);
                Console.Clear();
            }

            #endregion

            #region Fonction Sauvegarder

            void Sauvegarder()
            {
                List<string[]> ListeJoueurs2 = new List<string[]>();
                List<string> nouvelleListe = new List<string>();

                ListeJoueurs2.Clear();
                nouvelleListe.Clear();

                Joueur = File.ReadAllLines("InfosJoueurs.txt");

                if (Joueur.Length > 0)
                {
                    foreach (string item in Joueur)
                    {
                        InfosJoueurs = item.Split(':');
                        ListeJoueurs2.Add(InfosJoueurs);
                    }
                }

                foreach (string[] item in ListeJoueurs2)
                {

                    if (pseudo == item[0] && item.Length > 1)
                    {
                        item[2] = argent.ToString();
                    }

                    if (item.Length > 1)
                    {
                        infos = $"{item[0]}:{item[1]}:{item[2]}";
                    }

                    nouvelleListe.Add(infos);

                }
                File.Delete("InfosJoueurs.txt");
                File.WriteAllLines("InfosJoueurs.txt", nouvelleListe);
            }

            #endregion

            #region Fonction Effectuer un Dépôt

            float EffectueDepot(float argent2)
            {
                Console.Clear();
                Console.WriteLine("*EFFECTUER UN DÉPÔT*");
                Console.WriteLine();
                Console.Write("Quelle somme souhaitez vous déposer ? : ");
                string depot = Console.ReadLine();

                while (!float.TryParse(depot, out float thune2))
                {
                    Console.Write("Valeur incorrecte : ");
                    depot = Console.ReadLine();
                }

                float thune = float.Parse(depot);
                argent2 += thune;

                Console.Clear();

                return argent2;
            }

            #endregion

            #region Jeu

            while (jeu)
            {
                Sauvegarder();

                Console.WriteLine($"Votre cagnotte s'élève à {argent}$");

                #region Options 'Si' l'argent = 0

                if (argent == 0)
                {
                    gameOver();

                    Console.WriteLine("");
                    Console.WriteLine("-1- Effectuer un dépôt");
                    Console.WriteLine("-2- Quitter");
                    Console.WriteLine("");
                    Console.Write("Option : ");
                    option = Console.ReadLine();

                    if (int.TryParse(option, out choixOption))
                    {
                        choix = int.Parse(option);
                    }

                    while ((!int.TryParse(option, out int choixOption2)) || (choix != 1 && choix != 2))
                    {
                        Console.Write("Option incorrecte : ");
                        option = Console.ReadLine();

                        if (int.TryParse(option, out choixOption))
                        {
                            choix = int.Parse(option);
                        }
                    }

                    if (choix == 1)
                    {
                        argent = EffectueDepot(argent);
                    }

                    else if (choix == 2)
                    {
                        gameover = true;
                    }
                }

                #endregion

                #region Options 'Si' l'argent > 0

                else
                {
                    Console.WriteLine("");
                    Console.WriteLine("-1- Jouer");
                    Console.WriteLine("-2- Effectuer un dépôt");
                    Console.WriteLine("-3- Quitter");
                    Console.WriteLine("");
                    Console.Write("Option : ");
                    option = Console.ReadLine();

                    if (int.TryParse(option, out choixOption))
                    {
                        choix = int.Parse(option);
                    }

                    while ((!int.TryParse(option, out int choixOption2)) || (choix != 1 && choix != 2 && choix != 3))
                    {
                        Console.Write("Option incorrecte : ");
                        option = Console.ReadLine();

                        if (int.TryParse(option, out choixOption))
                        {
                            choix = int.Parse(option);
                        }
                    }

                    if (choix == 1)
                    {
                        Console.Clear();
                        Console.WriteLine($"Votre cagnotte s'élève à {argent}$");
                        Console.WriteLine();
                        Console.Write("Combien souhaitez-vous miser ? : ");
                        string miseString = Console.ReadLine();

                        while (!float.TryParse(miseString, out mise) || mise > argent)
                        {
                            Console.Write("Mise incorrecte, veuillez placer une autre mise : ");
                            miseString = Console.ReadLine();
                        }

                        argent -= mise;
                        Console.WriteLine("");
                        Console.WriteLine($"Vous avez misé {mise}$");
                        pause(2500);
                        Console.Clear();

                        tourDeRoue();
                    }

                    else if (choix == 2)
                    {
                        argent = EffectueDepot(argent);
                    }

                    else if (choix == 3)
                    {
                        gameover = true;
                    }
                }

                #endregion

                if (gameover)
                {
                    break;
                }
            }

            Console.WriteLine("");
            Console.WriteLine("Merci d'avoir joué !");
            Console.ReadKey();

            #endregion
        }
    }
}