﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Veuillez entrer un nombre de secondes à convertir : ");
            string nombre = Console.ReadLine();

            int nombreUser = int.Parse(nombre);

            int nombreHeures = nombreUser / 3600;
            nombreUser = nombreUser % 3600;

            int nombreMinutes = nombreUser / 60;
            nombreUser = nombreUser % 60;

            int nombreSecondes = nombreUser;

            Console.WriteLine($"Le nombre représente {nombreHeures} heures, {nombreMinutes} minutes et {nombreSecondes} secondes.");
            Console.ReadKey();
        }
    }
}
