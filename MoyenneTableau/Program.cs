﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoyenneTableau
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] tab = new int[5];
            int nombre;
            double total = 0;
            double moyenne = 0.0;
            Console.Write("Veuillez entrer 5 nombres : ");

            for (int i = 0, j = tab.Length - 1; i < tab.Length; i++, j--)
            {
                while (!int.TryParse(Console.ReadLine(), out nombre))
                {
                    Console.WriteLine("Entrée éronnée");
                }

                if (j > 0)
                    Console.Write($"Encore {j} nombres : ");
                tab[i] = nombre;
            }

            foreach (int i in tab)
            {
                total += i;
            }

            moyenne = total / tab.Length;

            Console.WriteLine($"La moyenne des nombres introduits est {moyenne}");
            Console.ReadKey();
        }
    }
}
