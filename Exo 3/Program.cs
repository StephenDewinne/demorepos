﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Exo_3
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] utilisateurs = new string[1];

            string user = "";

            Console.WriteLine("|Affichage automatique de la liste des utilisateurs|");

            if (File.Exists("utilisateurs.txt"))
            {
                utilisateurs = File.ReadAllLines("utilisateurs.txt");

                foreach (string item in utilisateurs)
                {
                    Console.WriteLine(item);
                }
            }

            else
            {
                File.Create("utilisateurs.txt");
            }

            bool ajouter = true;

            Console.Clear();

            while (ajouter)
            {
                Console.WriteLine("|Affichage automatique de la liste des utilisateurs|");

                if (File.Exists("utilisateurs.txt"))
                {
                    utilisateurs = File.ReadAllLines("utilisateurs.txt");
                }

                if (utilisateurs.Length == 0)
                {
                    Console.WriteLine("Aucun utilisateur");
                }

                else
                {
                    foreach (string item in utilisateurs)
                    {
                        Console.WriteLine(item);
                    }
                }

                Console.Write("Ajouter un utilisateur : ");

                user = Console.ReadLine();

                if (user == "Stop")
                {
                    ajouter = false;
                }

                else
                {
                    user = user + Environment.NewLine;
                    File.AppendAllText("utilisateurs.txt", user);
                }

                Console.Clear();
            }
        }
    }
}
