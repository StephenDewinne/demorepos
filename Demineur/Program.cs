﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demineur
{
    class Program
    {
        static void Main(string[] args)
        {

            //DEBUT DE L'INTRODUCTION

            Console.Write("Veuillez entrer un nombre de colonnes : ");
            string colonnes = Console.ReadLine();

            Console.Write("Veuillez entrer un nombre de rangées : ");
            string rangees = Console.ReadLine();

            Console.Write("Veuillez entrer le nombre de bombes : ");
            string bombes = Console.ReadLine();

            Console.WriteLine("");
            Console.WriteLine("Appuyer sur les flèches pour déplacer le curseur et sur [ENTREE] pour valider la case.");
            Console.WriteLine("");

            Console.WriteLine("Appuyez sur [ENTREE] pour commencer ...");
            Console.ReadKey();

            Console.Clear();

            //FIN DE L'INTRODUCTION



            //DEBUT DE DECLARATION DE VARIABLES

            int nombreColonnes = int.Parse(colonnes);
            int nombreRangees = int.Parse(rangees);
            int nombreBombes = int.Parse(bombes);

            int positionCurseurX = 0;
            int positionCurseurY = 0;

            int positionBombeX = 0;
            int positionBombeY = 0;

            int bombesProches = 0;

            int appuiCurseurX;
            int appuiCurseurY;

            var rand = new Random();

            bool jeu = true;

            string ligne = "";

            int gameOver = 0;

            char[,] TableauDeDepart = new char[nombreColonnes, nombreRangees];
            char[,] TableauDeValeurs = new char[nombreColonnes,nombreRangees];

                    //Initialisation du tableau de départ

            for (int i = 0; i < nombreRangees; i++)
            {
                for (int j = 0; j < nombreColonnes; j++)
                {
                    TableauDeDepart[j, i] = 'O';
                }
            }

                    //Initialisation du tableau sans valeurs

            for (int i = 0; i < nombreRangees; i++)
            {
                for (int j = 0; j < nombreColonnes; j++)
                {
                    TableauDeValeurs[j, i] = ' ';
                }
            }

                    //Disposition aléatoire des bombes

            for (int i = 0; i < nombreBombes; i++)
            {
                positionBombeX = rand.Next(nombreColonnes);
                positionBombeY = rand.Next(nombreRangees);

                while (TableauDeValeurs[positionBombeX, positionBombeY] == '*')
                {
                    positionBombeX = rand.Next(nombreColonnes);
                    positionBombeY = rand.Next(nombreRangees);
                }

                TableauDeValeurs[positionBombeX, positionBombeY] = '*';
            }

                    //Initialisation du tableau avec valeurs

            for (int i = 0; i < nombreRangees; i++)
            {
                for (int j = 0; j < nombreColonnes; j++)
                {
                    if (TableauDeValeurs[j, i] == '*')
                    {
                        continue;
                    }

                    if ((j - 1) >= 0)
                    {
                        if ((i - 1) >= 0)
                        {
                            if (TableauDeValeurs[j - 1, i - 1] == '*')
                            {
                                bombesProches += 1;
                            }
                        }

                        if ((i + 1) < nombreRangees)
                        {
                            if (TableauDeValeurs[j - 1, i + 1] == '*')
                            {
                                bombesProches += 1;
                            }
                        }

                        if (TableauDeValeurs[j - 1, i] == '*')
                        {
                            bombesProches += 1;
                        }
                    }

                    if (j >= 0)
                    {
                        if ((i - 1) >= 0)
                        {
                            if (TableauDeValeurs[j, i - 1] == '*')
                            {
                                bombesProches += 1;
                            }
                        }

                        if ((i + 1) < nombreRangees)
                        {
                            if (TableauDeValeurs[j, i + 1] == '*')
                            {
                                bombesProches += 1;
                            }
                        }
                    }

                    if ((j + 1) < nombreColonnes)
                    {
                        if ((i - 1) >= 0)
                        {
                            if (TableauDeValeurs[j + 1, i - 1] == '*')
                            {
                                bombesProches += 1;
                            }
                        }

                        if ((i + 1) < nombreRangees)
                        {
                            if (TableauDeValeurs[j + 1, i + 1] == '*')
                            {
                                bombesProches += 1;
                            }
                        }

                        if (TableauDeValeurs[j + 1, i] == '*')
                        {
                            bombesProches += 1;
                        }
                    }

                    if (bombesProches == 0)
                    {
                        TableauDeValeurs[j, i] = ' ';
                    }

                    else
                    {
                        TableauDeValeurs[j, i] = Convert.ToChar(bombesProches.ToString());
                    }

                    bombesProches = 0;
                }
            }

            ConsoleKeyInfo toucheAppuyee;

            //FIN DE DECLARATION DE VARIABLES



            //DECLARATION DE FONCTIONS

            void Dessin()
            {
                Console.SetCursorPosition(0, 0);

                for (int i = 0; i < nombreRangees; i++)
                {
                    for (int j = 0; j < nombreColonnes; j++)
                    {
                        ligne += TableauDeDepart[j, i];
                        ligne += " ";
                    }

                    Console.WriteLine(ligne);
                    ligne = "";
                }
            }

            void Deminage(int posX, int posY)
            {
                int posX2 = posX;
                int posY2 = posY;

                if (TableauDeValeurs[posX, posY] == ' ')
                {
                    TableauDeDepart[posX, posY] = TableauDeValeurs[posX, posY];

                    while (posY2 < nombreRangees && TableauDeValeurs[posX2, posY2] != '*')
                    {

                        while (posX2 < nombreColonnes && TableauDeValeurs[posX2, posY2] != '*')
                        {
                            TableauDeDepart[posX2, posY2] = TableauDeValeurs[posX2, posY2];

                            if (TableauDeValeurs[posX2, posY2] != ' ')
                            {
                                break;
                            }

                            posX2++;
                        }

                        posX2 = posX;

                        while (posX2 >= 0 && TableauDeValeurs[posX2, posY2] != '*')
                        {
                            TableauDeDepart[posX2, posY2] = TableauDeValeurs[posX2, posY2];

                            if (TableauDeValeurs[posX2, posY2] != ' ')
                            {
                                break;
                            }

                            posX2--;
                        }

                        posX2 = posX;

                        if (TableauDeValeurs[posX2, posY2] != ' ')
                        {
                            break;
                        }

                        posY2++;
                    }

                    posY2 = posY;

                    while (posY2 >= 0 && TableauDeValeurs[posX2, posY2] != '*')
                    {
                        if (TableauDeValeurs[posX2, posY2] != ' ')
                        {
                            break;
                        }

                        while (posX2 < nombreColonnes && TableauDeValeurs[posX2, posY2] != '*')
                        {
                            TableauDeDepart[posX2, posY2] = TableauDeValeurs[posX2, posY2];

                            if (TableauDeValeurs[posX2, posY2] != ' ')
                            {
                                break;
                            }

                            posX2++;
                        }

                        posX2 = posX;

                        while (posX2 >= 0 && TableauDeValeurs[posX2, posY2] != '*')
                        {
                            TableauDeDepart[posX2, posY2] = TableauDeValeurs[posX2, posY2];

                            if (TableauDeValeurs[posX2, posY2] != ' ')
                            {
                                break;
                            }

                            posX2--;
                        }

                        posX2 = posX;

                        if (TableauDeValeurs[posX2, posY2] != ' ')
                        {
                            break;
                        }

                        posY2--;

                    }

                    posY2 = posY;

                    while (posX2 < nombreColonnes && TableauDeValeurs[posX2, posY2] != '*')
                    {

                        while (posY2 < nombreRangees && TableauDeValeurs[posX2, posY2] != '*')
                        {
                            TableauDeDepart[posX2, posY2] = TableauDeValeurs[posX2, posY2];

                            if (TableauDeValeurs[posX2, posY2] != ' ')
                            {
                                break;
                            }

                            posY2++;
                        }

                        posY2 = posY;

                        while (posY2 >= 0 && TableauDeValeurs[posX2, posY2] != '*')
                        {
                            TableauDeDepart[posX2, posY2] = TableauDeValeurs[posX2, posY2];

                            if (TableauDeValeurs[posX2, posY2] != ' ')
                            {
                                break;
                            }

                            posY2--;
                        }

                        posY2 = posY;

                        if (TableauDeValeurs[posX2, posY2] != ' ')
                        {
                            break;
                        }

                        posX2++;
                    }

                    posX2 = posX;

                    while (posX2 >= 0 && TableauDeValeurs[posX2, posY2] != '*')
                    {

                        while (posY2 < nombreRangees && TableauDeValeurs[posX2, posY2] != '*')
                        {
                            TableauDeDepart[posX2, posY2] = TableauDeValeurs[posX2, posY2];

                            if (TableauDeValeurs[posX2, posY2] != ' ')
                            {
                                break;
                            }

                            posY2++;
                        }

                        posY2 = posY;

                        while (posY2 >= 0 && TableauDeValeurs[posX2, posY2] != '*')
                        {
                            TableauDeDepart[posX2, posY2] = TableauDeValeurs[posX2, posY2];

                            if (TableauDeValeurs[posX2, posY2] != ' ')
                            {
                                break;
                            }

                            posY2--;
                        }

                        posY2 = posY;

                        if (TableauDeValeurs[posX2, posY2] != ' ')
                        {
                            break;
                        }

                        posX2--;

                    }

                    posX2 = posX;
                }

                else if (TableauDeValeurs[posX, posY] == '*')
                {
                    TableauDeDepart[posX, posY] = TableauDeValeurs[posX, posY];
                    Dessin();
                    GameOver();
                }

                else
                {
                    TableauDeDepart[posX, posY] = TableauDeValeurs[posX, posY];
                }

                checkVictoire();
            }

            void GameOver()
            {
                Console.SetCursorPosition(0, nombreRangees + 2);
                Console.WriteLine("Game Over !");
                Console.WriteLine("");
                Console.WriteLine("Appuyer une n'importe quelle touche pour quitter ...");
                Console.ReadKey();
                gameOver = 1;
            }

            void victoire()
            {
                Console.SetCursorPosition(0, 0);

                for (int i = 0; i < nombreRangees; i++)
                {
                    for (int j = 0; j < nombreColonnes; j++)
                    {
                        ligne += TableauDeValeurs[j, i];
                        ligne += " ";
                    }

                    Console.WriteLine(ligne);
                    ligne = "";
                }

                Console.SetCursorPosition(0, nombreRangees + 2);
                Console.WriteLine("Félicitations ! Vous avez gagné !");
                Console.ReadKey();
                gameOver = 1;
            }

            void checkVictoire()
            {
                int casesRestantes = 0;

                for (int i = 0; i < nombreRangees; i++)
                {
                    for (int j = 0; j < nombreColonnes; j++)
                    {
                        if (TableauDeDepart[i, j] == 'O')
                        {
                            casesRestantes++;
                        }
                    }
                }

                if (casesRestantes == nombreBombes)
                {
                    victoire();
                    gameOver = 1;
                }
            }

            //FIN DE DECLARATION



            //DEBUT DU JEU

            Dessin();

            while (jeu)
            {
                Console.SetCursorPosition(positionCurseurX, positionCurseurY);
                toucheAppuyee = Console.ReadKey(true);

                if (toucheAppuyee.Key == ConsoleKey.RightArrow)
                {
                    positionCurseurX += 2;

                    if (positionCurseurX > (nombreColonnes - 1) * 2)
                    {
                        positionCurseurX = (nombreColonnes - 1) * 2;
                    }
                }

                if (toucheAppuyee.Key == ConsoleKey.LeftArrow)
                {
                    positionCurseurX -= 2;

                    if (positionCurseurX < 0)
                    {
                        positionCurseurX = 0;
                    }
                }

                if (toucheAppuyee.Key == ConsoleKey.UpArrow)
                {
                    positionCurseurY -= 1;

                    if (positionCurseurY < 0)
                    {
                        positionCurseurY = 0;
                    }
                }

                if (toucheAppuyee.Key == ConsoleKey.DownArrow)
                {
                    positionCurseurY += 1;

                    if (positionCurseurY > nombreRangees - 1)
                    {
                        positionCurseurY = nombreRangees - 1;
                    }
                }

                if (toucheAppuyee.Key == ConsoleKey.Enter)
                {
                    appuiCurseurX = Console.CursorLeft;
                    appuiCurseurY = Console.CursorTop;
                    Deminage(appuiCurseurX / 2, appuiCurseurY);

                    for (int i = 0; i < nombreRangees; i++)
                    {
                        for (int j = 0; j < nombreColonnes; j++)
                        {
                            if (TableauDeDepart[i, j] == ' ')
                            {
                                Deminage(i, j);
                            }
                        }
                    }

                    Console.Clear();
                    Dessin();
                }

                if (gameOver == 1)
                {
                    break;
                }
            }
            
            //FIN DU JEU

        }
    }
}