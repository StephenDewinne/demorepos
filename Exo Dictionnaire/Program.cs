﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;

namespace Exo_Dictionnaire
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> bar = new Dictionary<string, int>();

            string requete = "";
            int choix = 0;
            int nombreBoissons = 10;
            string[] carte = new string[1];
            List<string[]> Liste = new List<string[]>();

            if (File.Exists("Boissons.txt"))
            {
                carte = File.ReadAllLines("Boissons.txt");

                foreach (string item in carte)
                {
                    string[] words = item.Split(':');
                    Liste.Add(words);
                }

                foreach (string[] item in Liste)
                {
                    bar.Add(item[0], int.Parse(item[1]));
                }
            }

            else
            {
                File.Create("Boissons.txt");
            }

            void Ajouter()
            {
                Console.Write("Pour rajouter une boisson, écrivez son nom : ");
                requete = Console.ReadLine();

                if (!bar.TryGetValue(requete, out int nb))
                {
                    bar.Add(requete, nombreBoissons);
                    Console.WriteLine($"Boisson ajoutée : {requete} - Stock : {nombreBoissons}");
                }

                else
                {
                    Console.WriteLine("Cette boisson existe déjà dans notre stock.");
                }
            }

            void Modifier()
            {
                Console.Write("Pour modifier une quantité, écrivez le nom de la boisson : ");
                requete = Console.ReadLine();

                string change = "";

                if (bar.TryGetValue(requete, out int nb))
                {
                    Console.WriteLine($"{requete} - Quantité actuelle : {bar[requete]}");
                    Console.Write("Quelle quantité souhaitez vous mettre ? : ");

                    change = Console.ReadLine();

                    while (!int.TryParse(change, out int choi))
                    {
                        Console.Write("Option incorrecte : ");
                        change = Console.ReadLine();
                    }

                    bar[requete] = int.Parse(change);
                }

                else
                {
                    Console.WriteLine("Cette boisson n'existe pas dans notre stock.");
                }
            }

            void Supprimer()
            {
                Console.Write("Pour supprimer une boisson, écrivez son nom : ");
                requete = Console.ReadLine();

                if (bar.TryGetValue(requete, out int nb))
                {
                    bar.Remove(requete);
                    Console.WriteLine($"{requete} supprimée de la carte.");
                }

                else
                {
                    Console.WriteLine("Cette boisson n'existe pas dans notre stock.");
                }
            }

            void Sauvegarder()
            {
                List<string> nouvelleListe = new List<string>();
                string chaine = "";

                foreach (KeyValuePair<string, int> kvp in bar)
                {
                    chaine = $"{kvp.Key}:{kvp.Value.ToString()}";
                    nouvelleListe.Add(chaine);
                }

                File.WriteAllLines("Boissons.txt", nouvelleListe);

                Console.WriteLine("Sauvegarde réussie !");
                
            }

            void Afficher()
            {
                foreach (KeyValuePair<string, int> kvp in bar)
                {
                    Console.WriteLine($"{kvp.Key} : {kvp.Value}");
                }
            }

            void Options()
            {
                Console.WriteLine("Ajouter : '1'");
                Console.WriteLine("Modifier : '2'");
                Console.WriteLine("Supprimer : '3'");
                Console.WriteLine("Sauvegarder : '4'");
                Console.WriteLine("Afficher la carte : '5'");
                Console.WriteLine("Quitter : '6'");
                Console.Write("Quelle option ? : ");
            }

            
            while (choix != 6)
            {
                Options();
                requete = Console.ReadLine();

                while(!int.TryParse(requete, out int choi))
                {
                    Console.Write("Option incorrecte : ");
                    requete = Console.ReadLine();
                }

                choix = int.Parse(requete);

                Console.Clear();

                if (choix == 1)
                {
                    Ajouter();
                }

                else if (choix == 2)
                {
                    Modifier();
                }

                else if (choix == 3)
                {
                    Supprimer();
                }

                else if (choix == 4)
                {
                    Sauvegarder();
                }

                else if (choix == 5)
                {
                    Afficher();
                }

                Console.WriteLine(" ");
            }
        }
    }
}
