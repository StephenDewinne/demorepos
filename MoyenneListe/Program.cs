﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoyenneListe
{
    class Program
    {
        static void Main(string[] args)
        {
            List<double> liste = new List<double>();
            int nombre = 0;
            double total = 0;
            double moyenne = 0;
            int compte = 0;
            Console.Write("Entrez autant de nombres que vous voulez et 'ok' pour arrêter : ");

            while (int.TryParse(Console.ReadLine(), out nombre))
            {
                liste.Add(nombre);
                Console.Write("Entrez encore quelque chose : ");
            }

            foreach (double i in liste)
            {
                total += i;
                compte++;
            }

            moyenne = total / compte;
            Console.WriteLine($"La moyenne est de {moyenne}");
            Console.ReadKey();
        }
    }
}
