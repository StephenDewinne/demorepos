﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice_POO
{
    class Program
    {
        static void Main(string[] args)
        {
            Personne client = new Personne();
            Courant compte = new Courant();
            Banque banque = new Banque();

            string _option;
            int option = 0;
            string _montant;
            double montant = 0;
            string numeroDeCompte = "";


            while (option != 3 && option != 4)
            {
                Console.WriteLine("-1- CRÉER UN COMPTE");
                Console.WriteLine("-2- SUPPRIMER UN COMPTE");
                Console.WriteLine("-3- CHOISIR UN COMPTE");
                Console.WriteLine("-4- QUITTER");
                Console.WriteLine("");
                Console.Write("   Option : ");
                _option = Console.ReadLine();
                option = int.Parse(_option);

                Console.Clear();

                if (option == 1)
                {
                    banque.AjouterCompte();
                }

                if (option == 2)
                {
                    if (banque.Comptes.Count > 0)
                    {
                        foreach (KeyValuePair<string, Courant> item in banque.Comptes)
                        {
                            Console.WriteLine($"{item.Key} - {item.Value.Titulaire.Nom} {item.Value.Titulaire.Prenom}");
                        }

                        Console.WriteLine("");

                        while (!banque.Comptes.ContainsKey(numeroDeCompte))
                        {
                            Console.Write("Quel numéro de compte voulez-vous supprimer ? : ");
                            numeroDeCompte = Console.ReadLine();
                        }

                        banque.SupprimerCompte(numeroDeCompte);
                    }

                    else
                    {
                        Console.WriteLine("Vous n'avez aucun compte chez nous");
                        option = 0;
                        System.Threading.Thread.Sleep(2000);
                    }
                }

                if (option == 3)
                {
                    if (banque.Comptes.Count > 0)
                    {

                        foreach (KeyValuePair<string, Courant> item in banque.Comptes)
                        {
                            Console.WriteLine($"{item.Key} - {item.Value.Titulaire.Nom} {item.Value.Titulaire.Prenom}");
                        }

                        Console.WriteLine("");

                        while (!banque.Comptes.ContainsKey(numeroDeCompte))
                        {
                            Console.Write("Quel numéro de compte voulez-vous choisir ? : ");
                            numeroDeCompte = Console.ReadLine();
                        }

                        compte = banque.ChoisirCompte(numeroDeCompte);
                    }

                    else
                    {
                        Console.WriteLine("Vous n'avez aucun compte chez nous");
                        option = 0;
                        System.Threading.Thread.Sleep(2000);
                    }
                }

                Console.Clear();
            }

            if (option == 3)
            {
                option = 0;
            }

            if (option == 4)
            {
                option = 3;
            }

            while (option != 3)
            {
                Console.Clear();
                option = 0;
                montant = 0;
                Console.WriteLine($"TITULAIRE : {compte.Titulaire.Nom} {compte.Titulaire.Prenom}    |    COMPTE : {compte.Numero}    |    SOLDE : {compte.Solde} $    |    LIMITE DE CRÉDIT : {compte.LigneDeCredit} $");
                Console.WriteLine("");
                Console.WriteLine("-1- DEPOT");
                Console.WriteLine("-2- RETRAIT");
                Console.WriteLine("-3- QUITTER");
                Console.WriteLine("");

                Console.Write("   Option : ");
                _option = Console.ReadLine();
                int.TryParse(_option, out option);

                while (option != 1 && option != 2 && option != 3)
                {
                    Console.Write(" Option incorrecte : ");
                    _option = Console.ReadLine();
                    int.TryParse(_option, out option);
                }

                Console.WriteLine("");

                if (option == 1)
                {
                    do
                    {
                        Console.Write("Quelle somme souhaitez vous déposer ? : ");
                        _montant = Console.ReadLine();
                    }

                    while (!double.TryParse(_montant, out montant));

                    compte.Depot(montant);
                }

                Console.WriteLine("");

                if (option == 2)
                {
                    do
                    {
                        Console.Write("Quelle somme souhaitez vous retirer ? : ");
                        _montant = Console.ReadLine();
                    }

                    while (!double.TryParse(_montant, out montant));


                    compte.Retrait(montant);
                }

            }
        }
    }
}
