﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice_POO
{
    class Banque
    {
        private string _nom;

        public Dictionary<string, Courant> Comptes = new Dictionary<string, Courant>();

        public string Nom
        {
            get
            {
                return _nom;
            }

            set
            {
                _nom = value;
            }
        }

        public void AjouterCompte()
        {
            Personne client = new Personne();
            Courant compte = new Courant();

            Console.WriteLine("Bienvenue dans l'assisstant de création de compte.");
            Console.WriteLine("");

            Console.Write("Pour commencer, quel est votre nom ? : ");
            client.Nom = Console.ReadLine();

            Console.WriteLine("");

            Console.Write("Ensuite, quel est votre prénom ? : ");
            client.Prenom = Console.ReadLine();

            Console.WriteLine("");

            /* DEMANDE DE DATE DE NAISSANCE
            string _annee;
            string _mois;
            string _jour;

            int annee;
            int mois;
            int jour;

            do
            {
                Console.Write("Quelle est votre année de naissance ? : ");
                _annee = Console.ReadLine();
            }

            while (!int.TryParse(_annee, out annee));

            Console.WriteLine("");

            do
            {
                Console.Write("Quel est votre mois de naissance ? : ");
                _mois = Console.ReadLine();
            }

            while (!int.TryParse(_mois, out mois));

            Console.WriteLine("");

            do
            {
                Console.Write("Quel est votre jour de naissance ? : ");
                _jour = Console.ReadLine();
            }

            while (!int.TryParse(_jour, out jour));

            Console.WriteLine("");

            client.DateNaiss = new DateTime(annee, mois, jour);
            */

            var rand = new Random();


            compte.Numero = $"BE{rand.Next(0, 10)}{rand.Next(0, 10)} {rand.Next(0, 10)}{rand.Next(0, 10)}{rand.Next(0, 10)}{rand.Next(0, 10)} {rand.Next(0, 10)}{rand.Next(0, 10)}{rand.Next(0, 10)}{rand.Next(0, 10)} {rand.Next(0, 10)}{rand.Next(0, 10)}{rand.Next(0, 10)}{rand.Next(0, 10)}";

            string _credit;
            int credit;

            do
            {
                Console.Write("Quel montant de crédit maximum souhaitez-vous ? : ");
                _credit = Console.ReadLine();
            }

            while (!int.TryParse(_credit, out credit));

            Console.WriteLine("");

            compte.Titulaire = client;

            Console.WriteLine($"Nous vous avons attribué le compte {compte.Numero}");

            Console.WriteLine("");

            if (credit > 0)
            {
                credit *= -1;
            }

            compte.LigneDeCredit = credit;

            Comptes.Add(compte.Numero, compte);

            Console.ReadKey();
        }

        public void SupprimerCompte(string numeroDeCompte)
        {
            Comptes.Remove(numeroDeCompte);
        }

        public Courant ChoisirCompte(string numeroDeCompte)
        {
            foreach (KeyValuePair<string, Courant> item in Comptes)
            {
                if (item.Key == numeroDeCompte)
                {
                    return item.Value;
                }
            }

            return null;
        }
    }
}
