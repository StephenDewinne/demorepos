﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice_POO
{
    class Personne
    {
        private string _nom;

        private string _prenom;

        private DateTime _dateNaiss;

        public string Nom
        {
            get
            {
                return _nom;
            }

            set
            {
                _nom = value;
            }
        }

        public string Prenom
        {
            get
            {
                return _prenom;
            }

            set
            {
                _prenom = value;
            }
        }

        public DateTime DateNaiss
        {
            get
            {
                return _dateNaiss;
            }

            set
            {
                _dateNaiss = value;
            }
        }
    }
}
