﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice_POO
{
    class Courant
    {
        private string _numero;

        private double _solde = 0;

        private double _ligneDeCredit;

        private Personne _titulaire;

        public string Numero
        {
            get
            {
                return _numero;
            }

            set
            {
                _numero = value;
            }
        }

        public double Solde
        {
            get
            {
                return _solde;
            }

            private set
            {
                
            }
        }

        public double LigneDeCredit
        {
            get
            {
                return _ligneDeCredit;
            }

            set
            {
                _ligneDeCredit = value;
            }
        }

        public Personne Titulaire
        {
            get
            {
                return _titulaire;
            }

            set
            {
                _titulaire = value;
            }
        }

        public void Retrait(double Montant)
        {
            if ((this.Solde - Montant) < this.LigneDeCredit)
            {
                Console.WriteLine("");
                Console.WriteLine("Impossible de retirer la somme - Ligne de crédit atteinte");
                Console.WriteLine("");
                System.Threading.Thread.Sleep(2000);
            }

            else
            {
                _solde -= Montant;
            }
        }

        public void Depot(double Montant)
        {
            _solde += Montant;
        }
    }
}
